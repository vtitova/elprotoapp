import { Component } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';

import { MemorialItemPage } from '../memorial_item/memorial_item';
import { MemorialAddPage } from '../memorial_item/memorial_item_add';

import { MemorialItems } from '../../providers/providers';
import { MemorialItem } from '../../models/memorial_item';

@Component({
  selector: 'page-memorial',
  templateUrl: 'memorial.html'
})
export class MemorialPage {
  currentItems: MemorialItem[];

  constructor(public navCtrl: NavController, public items: MemorialItems,  public modalCtrl: ModalController) {
    var currentItems = this.items.query();
    this.currentItems = currentItems.map(function(item) {
      item.person['birth_year'] = getYearFromDate(item.person['birth_date']);
      item.person['death_year'] = getYearFromDate(item.person['death_date']);
      return item;
    });
    function getYearFromDate(date){
      date = new Date(date);
      return date.getFullYear();
    }
  }

/**
 * The view loaded, let's query our items for the list
 */
  ionViewDidLoad() {
  }

  openMemorialItem(memorialitem){
    this.navCtrl.push(MemorialItemPage, { 'memorialitem' : memorialitem } );
  }

  openAddMemorialModal() {
    let modal = this.modalCtrl.create(MemorialAddPage);
    modal.present();

    return false;
  }
}
