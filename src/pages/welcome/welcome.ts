import { Component } from '@angular/core';
import { ModalController, NavController, ToastController } from 'ionic-angular';

import { TranslateService } from 'ng2-translate/ng2-translate';

import { SignupPage } from '../signup/signup';
import { MainPage } from '../../pages/pages';
import { User } from '../../providers/user';
import { ModalContentPage } from './modal-content';

/**
 * The Welcome Page is a splash page that quickly describes the app,
 * and then directs the user to create an account or log in.
 * If you'd like to immediately put the user onto a login/signup page,
 * we recommend not using the Welcome page.
*/
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})
export class WelcomePage {
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: {phone: string, password: string} = {
    phone: '',
    password: ''
  };

  // Our translated text strings
  private loginErrorString: string;

  constructor(public navCtrl: NavController,
              public user: User,
              public toastCtrl: ToastController,
              public translateService: TranslateService,
              public modalCtrl: ModalController) {
    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })
  }

  openAgreementModal() {
    let modal = this.modalCtrl.create(ModalContentPage);
    modal.present();
    return false;
  }

  // Attempt to login in through our User service
  doLogin() {
    this.user.login(this.account).subscribe((resp) => {
      this.navCtrl.push(MainPage);
    }, (err) => {
      this.navCtrl.push(MainPage);
      // Unable to log in
      // let toast = this.toastCtrl.create({
      //   message: this.loginErrorString,
      //   duration: 3000,
      //   position: 'top'
      // });
      // toast.present();
    });
  }

  signup() {
    this.navCtrl.push(SignupPage);
  }

  iagree(){
    console.log('i agree!');
    return false;
  }
}

