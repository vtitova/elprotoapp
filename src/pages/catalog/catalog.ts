import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-catalog',
  templateUrl: 'catalog.html'
})
export class CatalogPage {
  catalogItems: any;

  constructor(public navCtrl: NavController, navParams: NavParams) {
    this.catalogItems = [
      {
        name: 'Пластиковые маки',
        description: 'Пластиковые маки долговечнее тканевых и выглядят более натурально.',
        price: '1550₽',
        image: 'assets/img/plastic_mak.jpg'
      },
      {
        name: 'Можжевельник',
        description: 'Можжевельник казацкий – стелящийся кустарник с темной хвоей, один из лучших вариантов для озеленения захоронений. Он переносит жару и морозы, хорошо сочетается с мрамором, гранитом и другими материалами.',
        price: '3990₽',
        image: 'assets/img/mozzevelnik-640x480.jpg'
      },
      {
        name: 'Хосты',
        description: 'Хосты не болеют, хорошо переносят холода, могут расти в тени и, разрастаясь, образуют пышные кустики, меж которых сорнякам не остаётся места.',
        price: '2750₽',
        image: 'assets/img/hosty-640x426.jpg'
      },
      {
        name: 'Безвременники',
        description: 'Достоинство травянистых многолетников в том, что их не нужно обновлять каждый год.',
        price: '1750₽',
        image: 'assets/img/00078188.jpg'
      },
      {
        name: 'Камнеломки',
        description: 'Камнеломки способны помочь в озеленении даже самых проблемных участков, хорошо сочетающиеся с разными породами камня и помогающие драпировать и само надгробие трогательными побегами.',
        price: '2250₽',
        image: 'assets/img/93d01892d3cd0a45db96e21b26fe0f7b_i-400.jpg'
      },
    ];
  }
}
