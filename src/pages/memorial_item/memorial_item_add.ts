import { Component } from '@angular/core';
import { Platform, NavParams, ViewController } from 'ionic-angular';

@Component({
  templateUrl: 'memorial_item_add.html'
})
export class MemorialAddPage {
  character;

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController
  ) { }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
