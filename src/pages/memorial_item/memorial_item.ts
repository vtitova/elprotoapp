import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { MapPage } from '../map/map';
import { CatalogPage } from '../catalog/catalog';

@Component({
  selector: 'page-memorialitem-detail',
  templateUrl: 'memorial_item.html'
})
export class MemorialItemPage {
  item: any;

  constructor(public navCtrl: NavController, navParams: NavParams) {
    this.item = navParams.get('memorialitem');
  }

  openMemorialItemMap(coordinates){
    this.navCtrl.push(MapPage, { 'coordinates': coordinates } );
  }

  openCatalogPage(){
    this.navCtrl.push(CatalogPage, {} );
  }

  ifSlider(item){
    return item.burial.pictures && item.burial.pictures.length > 0;
  }
}
