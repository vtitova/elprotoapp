import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate/ng2-translate';

import { MemorialPage } from '../memorial/memorial';
/*
  Generated class for the Cards page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'main-menu',
  templateUrl: 'main_menu.html'
})
export class MainMenuPage {
  cardItems: any[];

  // Our translated text strings
  private titlememorial: string;
  private titleservice: string;
  private titledeath: string;

  constructor(public navCtrl: NavController,
              public translateService: TranslateService) {


    translateService.get(['MENUMEMORIAL', 'MENUSERVICE', 'MENUDEATH']).subscribe(values => {
      this.titlememorial = values['MENUMEMORIAL'];
      this.titleservice = values['MENUSERVICE'];
      this.titledeath = values['MENUDEATH'];
    });
                
    this.cardItems = [
      {
        image: 'assets/img/rit1.jpg',
        title: this.titlememorial,
        click: 'goToMemorial'
      },
      {
        image: 'assets/img/rit2.jpg',
        title: this.titleservice,
        click: 'goToMemorial'
      },
      {
        image: 'assets/img/rit3.jpg',
        title: this.titledeath,
        click: 'goToMemorial'
      }
    ];

  }
  goToMemorial(){
    this.navCtrl.push(MemorialPage, {});
  }
}
