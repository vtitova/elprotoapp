import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { MemorialItem } from '../../models/memorial_item';

@Injectable()
export class MemorialItems {
  items: MemorialItem[] = [];

  constructor(public http: Http) {
    let items = [
      {
        person: {
          picture: 'assets/img/LSVygotsky.jpg',
          name: 'Лев Семёнович Выготский',
          birth_date: new Date('11.17.1896'),
          death_date: new Date('06.11.1934')
        },
        burial: {
          cemetery: 'Новодевичье кладбище',
          pictures: [
            'assets/img/Vygotskij-1.jpg'
          ],
          coordinates: [55.7248999, 37.5540997]
        }
      },
      {
        person: {
          picture: 'assets/img/Edgar_Allan_Poe.jpg',
          name: 'Эдгар Аллан По',
          birth_date: new Date('01.19.1809'),
          death_date: new Date('10.01.1849')
        },
        burial: {
          cemetery: 'Вестминтерское кладбище',
          pictures: [
            'assets/img/210px-EdgarAllanPoeGrave.jpg',
            'assets/img/adgar_poe_grave_Baltimore.jpg'
          ],
          coordinates: [39.2902428, -76.6231604]
        }
      },
      {
        person: {
          picture: 'assets/img/Hovard.jpg',
          name: 'Говард Лавкрафт',
          birth_date: new Date('08.20.1890'),
          death_date: new Date('03.15.1937')
        },
        burial: {
          cemetery: 'Кладбище Суон Пойнт',
          coordinates: [41.8536551, -71.385889]
        }
      },
      {
        person: {
          picture: 'assets/img/stieve.jpeg',
          name: 'Стив Джобс',
          birth_date: new Date('02.24.1955'),
          death_date: new Date('10.05.2011')
        },
        burial: {
          cemetery: 'Мемориальный парк Альта-Меса',
          pictures: [
            'assets/img/9a1e8a9c20e2a7.jpg'
          ],
          coordinates: [37.3994657, -122.1293449]
        }
      }
    ];

     for(let item of items) {
       this.items.push(new MemorialItem(item));
     }
  }

  query(params?: any) {
    if(!params) {
      return this.items;
    }

    return this.items.filter((item) => {
      for(let key in params) {
        let field = item[key];
        if(typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return item;
        } else if(field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  add(item: MemorialItem) {
    this.items.push(item);
  }

  delete(item: MemorialItem) {
    this.items.splice(this.items.indexOf(item), 1);
  }
}
