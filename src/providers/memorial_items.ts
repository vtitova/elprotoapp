import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Api } from './api';

import { MemorialItem } from '../models/memorial_item';

@Injectable()
export class MemorialItems {

  constructor(public http: Http, public api: Api) {
  }

  query(params?: any) {
    return this.api.get('/items', params)
      .map(resp => resp.json());
  }

  add(item: MemorialItem) {
  }

  delete(item: MemorialItem) {
  }

}
