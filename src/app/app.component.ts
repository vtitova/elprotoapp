import { Component, ViewChild } from '@angular/core';
import {Platform, Nav, Config} from 'ionic-angular';
import { StatusBar } from 'ionic-native';

import { SplashScreen } from '@ionic-native/splash-screen';

import { FirstRunPage } from '../pages/pages';

import { TranslateService } from 'ng2-translate/ng2-translate';

@Component({
  template: `<ion-menu [content]="content">
    <ion-header>
      <ion-toolbar>
        <ion-title>{{ 'MENU' | translate }}</ion-title>
      </ion-toolbar>
    </ion-header>

    <ion-content>
      <ion-list>
        <button ion-item *ngFor="let p of menupages" (click)="this[item.click]()">
        <ion-icon name="{{p.icon}}"></ion-icon>
          {{p.title | translate }}
        </button>
      </ion-list>
    </ion-content>

  </ion-menu>
  <ion-nav #content [root]="rootPage"></ion-nav>`
})
export class MyApp {
  rootPage = FirstRunPage;

  @ViewChild(Nav) nav: Nav;

  menupages: any[] = [
    { title: 'PROFILE', icon: 'md-person', onclick: 'exitApp' },
    { title: 'SETTINGS_TITLE', icon: 'md-settings', onclick: 'exitApp' },
    { title: 'EXIT', icon: 'md-exit', onclick: 'exitApp' }
  ]

  constructor(
    translate: TranslateService,
    config: Config,
    private platform: Platform,
    public _SplashScreen: SplashScreen
  ) {
    this.platform = platform;
    // Set the default language for translation strings, and the current language.
    translate.setDefaultLang('ru');
    translate.use('ru')

    translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      setTimeout(() => {
        this._SplashScreen.hide();
      }, 100);
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  exitApp(){
    //  this.nav.app.exitApp();
  }
}
