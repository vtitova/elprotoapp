import { NgModule, ErrorHandler } from '@angular/core';
import { Http } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen';

import { MyApp } from './app.component';

import { MapPage } from '../pages/map/map';
import { SignupPage } from '../pages/signup/signup';
import { WelcomePage } from '../pages/welcome/welcome';
import { ModalContentPage } from '../pages/welcome/modal-content';
import { MemorialAddPage } from '../pages/memorial_item/memorial_item_add';
import { MainMenuPage } from '../pages/main_menu/main_menu';
import { MemorialPage } from '../pages/memorial/memorial';
import { MemorialItemPage } from '../pages/memorial_item/memorial_item';
import { CatalogPage } from '../pages/catalog/catalog';
import { User } from '../providers/user';
import { Api } from '../providers/api';
import { MemorialItems } from '../mocks/providers/memorial_items';

import { BrowserModule } from '@angular/platform-browser';

import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate/ng2-translate';

import { TextMaskModule } from 'angular2-text-mask';
import { IonicImageViewerModule } from 'ionic-img-viewer';

// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function createTranslateLoader(http: Http) {
  return new TranslateStaticLoader(http, './assets/i18n', '.json');
}

/**
 * The Pages array lists all of the pages we want to use in our app.
 * We then take these pages and inject them into our NgModule so Angular
 * can find them. As you add and remove pages, make sure to keep this list up to date.
 */
let pages = [
  MyApp,
  MapPage,
  SignupPage,
  WelcomePage,
  ModalContentPage,
  MemorialAddPage,
  MainMenuPage,
  MemorialPage,
  MemorialItemPage,
  CatalogPage
];

export function declarations() {
  return pages;
}

export function entryComponents() {
  return pages;
}

export function providers() {
  return [
    User,
    Api,
    MemorialItems,
    SplashScreen,
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ];
}

@NgModule({
  declarations: declarations(),
  imports: [
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: (createTranslateLoader),
      deps: [Http]
    }),
    BrowserModule,
    TextMaskModule,
    IonicImageViewerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: entryComponents(),
  providers: providers()
})
export class AppModule {}
