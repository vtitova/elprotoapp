export class MemorialItem {
  person: Object;
  burial: Object;

  constructor(
    params: Object
  ) {
    this.person = params['person'];
    this.burial = params['burial'];
  }

}
