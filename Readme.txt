Видео работы приложения:
https://youtu.be/Jl0iQZhH53M

Запуск на локалке:

1. Устанавливаем ionic
npm install -g ionic cordova

2. Клонируем репозиторий
git clone https://vtitova@bitbucket.org/vtitova/elprotoapp.git 

3. cd elprotoapp

4. npm install

5. ionic serve


Запуск на android-устройстве:

1. Установить на компьютер
 * Java JDK
 * Android Studio
 * из Android Studio обновить Android SDK Tools

2. Включить на устройстве developer mode и подключить устройство по usb

3. Добавляем платформу

cordova platform add android
(ionic cordova build android)?

4. Собирам и запускаем приложение на устройстве
ionic run android --prod --device

Вспомогательные команды:

При сборке приложения на устройство могут возникать ошибки а-ля "не удаётся найти устройство"

1. Смотрим список подключенных устройств:
adb devices

2. Останавливаем сервис adb:
adb kill-server

3. И запускаем снова:
adb start-server